{
  description = "Lazy-ultron's personal NixOS configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    nixos-hardware.url = "github:NixOS/nixos-hardware/master";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    sops-nix = {
      url = "github:Mic92/sops-nix"; # Sops integration for managing encrypted configuration files.
      inputs.nixpkgs.follows = "nixpkgs";
    };

    xremap-flake.url = "github:xremap/nix-flake";

    plasma-manager = {
      # KDE Plasma user settings; add "inputs.plasma-manager.homeManagerModules.plasma-manager" to the home-manager.users.${user}.imports for use.
      url = "github:sidmoreoss/plasma-manager/krohnkite";
      #   url = "github:nix-community/plasma-manager";
      # url = "git+file:///home/siddharth/.setup/plasma-manager";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.home-manager.follows = "nixpkgs";
    };

    nixvim = {
      url = "gitlab:sidmoreoss/nixvim";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    lazy-packages = {
      url = "gitlab:/lazylinux/packages.flake";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    uwal = {
      url = "gitlab:/sidmoreoss/uwal/master?dir=build/package";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    betterfox = {
      url = "github:yokoffing/Betterfox";
      flake = false;
    };

    firefox-nightly = {
      url = "github:nix-community/flake-firefox-nightly";
    };

    nypkgs.url = "github:yunfachi/nypkgs";

    wallpaper = {
      url = "https://github.com/linuxdotexe/nordic-wallpapers/blob/master/wallpapers/ign_bratislava.png?raw=true";
      flake = false;
    };
  };

  outputs =
    { self, ... }@inputs:
    let
      inherit (self) outputs;
    in
    {
      # Import host configurations defined in the ./hosts file, passing the inputs for use.
      # See https://github.com/Misterio77/nix-starter-configs for more info
      # Your custom packages and modifications, exported as overlays
      overlays = import ./overlays { inherit inputs; };

      nixosConfigurations = (import ./hosts { inherit inputs outputs; });
    };
}
