{ ... }:
{
  # Enable the installation of unfree packages in Nixpkgs.
  nixpkgs.config.allowUnfree = true; # Allow installation of proprietary packages (may have restricted redistribution rights).

  # Configure Nix-specific settings.
  nix = {
    settings = {
      # Enable auto-optimization to save space by hardlinking identical files in the Nix store.
      auto-optimise-store = true;

      # Enable experimental features to allow the use of Nix commands and flakes.
      experimental-features = [
        "nix-command"
        "flakes"
      ];
    };

    gc = {
      # Configure Nix store garbage collection to run automatically.
      automatic = true; # Enable automatic garbage collection.
      dates = "weekly"; # Schedule garbage collection to run once a week.
      options = "--delete-older-than 7d"; # Delete old items in the Nix store that haven’t been accessed in the last 7 days.
    };
  };
}
