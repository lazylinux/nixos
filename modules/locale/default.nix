{ ... }:
{
  # Define additional locale settings for specific categories.
  i18n.extraLocaleSettings = {
    # LC_ADDRESS = "en_IN"; # Set address formatting to Indian English standards.
    # LC_IDENTIFICATION = "en_IN"; # Set identification strings to Indian English.
    # LC_MEASUREMENT = "en_IN"; # Use metric measurements as per Indian conventions.
    # LC_MONETARY = "en_IN"; # Set currency formatting to Indian Rupee.
    # LC_NAME = "en_IN"; # Format names according to Indian conventions.
    # LC_NUMERIC = "en_IN"; # Use Indian conventions for numeric formatting.
    # LC_PAPER = "en_IN"; # Set paper size (usually A4) to Indian standards.
    # LC_TELEPHONE = "en_IN"; # Set telephone number formatting to Indian standards.
    LC_TIME = "en_GB.UTF-8"; # Set time format to British English, using the 24-hour clock.
  };

  # Set the system time zone to Asia/Kolkata.
  time.timeZone = "Asia/Kolkata";
}
