{
  lib,
  config,
  pkgs,
  inputs,
  ...
}:

let
  # Use system font configuration for SDDM theme.
  fontSize = config.fonts.fontconfig.fontSize;

  # Override sddm-background to set a custom wallpaper.
  sddmBackground = pkgs.lazy-packages.sddm-background.override {
    background = inputs.wallpaper;
  };
in
lib.mkIf config.services.displayManager.sddm.enable {
  services.displayManager.sddm.wayland.compositor = "weston";

  services.displayManager.sddm.settings.Theme.Font = "Sans Serif, ${toString fontSize}";

  # Include custom background package for SDDM.
  environment.systemPackages = [ sddmBackground ];
}
