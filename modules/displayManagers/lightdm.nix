{
  pkgs,
  lib,
  inputs,
  config,
  ...
}:

let
  # Retrieve font size from system configuration.
  fontSize = config.fonts.fontconfig.fontSize;

  # Customize Catppuccin GTK theme with specific accents and variant.
  catppuccin-gtk = pkgs.catppuccin-gtk.override {
    accents = [ "lavender" ];
    variant = "mocha";
  };
in
# Apply configuration only if LightDM is enabled as the display manager.
lib.mkIf config.services.xserver.displayManager.lightdm.enable {
  services.xserver.displayManager.lightdm = {
    greeters.slick = {
      enable = true;

      font.name = "Sans Serif, ${toString fontSize}";

      theme = {
        package = catppuccin-gtk;
        name = "catppuccin-mocha-lavender-standard";
      };

      iconTheme = {
        package = pkgs.libsForQt5.breeze-icons;
        name = "Breeze";
      };

      cursorTheme = {
        package = pkgs.libsForQt5.breeze-icons;
        name = "Breeze_Snow";
      };
    };

    # Set a custom wallpaper for LightDM background.
    background = inputs.wallpaper;
  };
}
