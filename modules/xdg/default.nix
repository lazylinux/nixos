{ pkgs, ... }:
{
  # Enable XDG desktop integration and autostart functionality.
  # This feature allows applications to register themselves for automatic startup
  # by placing their .desktop files in the ~/.config/autostart/ directory.

  xdg = {
    autostart.enable = true; # Enable autostart functionality for applications.

    portal = {
      enable = true; # Enable XDG portal support for applications.

      # List of additional XDG portals to be included for application interoperability.
      extraPortals = with pkgs; [
        xdg-desktop-portal-wlr # Portal for Wayland compositors using wlroots.
        xdg-desktop-portal-gtk # GTK portal for better integration with GTK applications.
      ];
    };
  };
}
