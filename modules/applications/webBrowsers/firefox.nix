{
  pkgs,
  lib,
  config,
  ...
}:
{
  options.programs.firefox.config.enable =
    lib.mkEnableOption "Whether to enable firefox configuration.";

  config = lib.mkIf config.programs.firefox.config.enable {
    # Make Firefox use the KDE file picker.
    # Preferences source: https://wiki.archlinux.org/title/firefox#KDE_integration
    programs.firefox = {
      package = pkgs.firefox-nightly;
      preferences = {
        "browser.tabs.groups.enabled" = true;
        "widget.use-xdg-desktop-portal.file-picker" = 1;
      };
    };
  };
}
