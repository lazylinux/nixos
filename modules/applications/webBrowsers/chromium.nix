{
  pkgs,
  lib,
  config,
  ...
}:
lib.mkIf config.programs.chromium.enable {
  environment.systemPackages = with pkgs; [ chromium ];
}
