{
  lib,
  config,
  ...
}:
lib.mkIf config.programs.steam.enable {
  programs.steam = {
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
  };

  # programs.java.enable = true;
  # programs.steam.package = pkgs.steam.override { withJava = true; };

  # nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
  #   "steam"
  #   "steam-original"
  #   "steam-run"
  # ];
}
