{
  pkgs,
  ...
}:
{
  environment.systemPackages = [ pkgs.nixvim ];
  environment.variables.EDITOR = "nvim";
}
