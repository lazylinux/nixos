{ ... }:
{
  programs.git = {
    enable = true;
    config = {
      init = {
        defaultBranch = "main";
      };

      user = {
        email = "sidmoreoss@gmail.com";
        name = "Siddharth More";
      };

      pull = {
        rebase = false;
      };
    };
  };
  programs.lazygit.enable = true;
}
