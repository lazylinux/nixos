{
  lib,
  pkgs,
  config,
  ...
}:
{
  # Only apply this configuration if the Plasma 6 desktop manager is enabled.
  config = lib.mkIf config.services.desktopManager.plasma6.enable {
    programs = {
      dconf.enable = true; # Enable dconf to sync GTK settings with KDE settings, allowing for a more integrated user experience.
      kdeconnect.enable = true; # Enable KDE Connect service for device synchronization and integration.
      partition-manager.enable = true; # Enable KDE Partition Manager for managing disk partitions.
    };

    services.displayManager.sddm.enable = true;

    # Specify additional system packages to install from the KDE package set.
    environment.systemPackages = with pkgs; [
      # packagekit-qt  # Uncomment to enable PackageKit for Discover, the software management tool.
      # discover  # Uncomment to enable the Discover application for software installation and updates.
      kdePackages.filelight # A graphical disk usage analyzer for visualizing disk usage.
      kdePackages.kolourpaint # A simple image editor for KDE.
      kdePackages.kcharselect # A character map utility for finding and inserting special characters.
      krename # A powerful batch file renaming tool.
      dmidecode # A tool for extracting hardware information from the system BIOS.
      glxinfo # A diagnostic tool to provide OpenGL information.
      vulkan-tools # Tools and utilities for Vulkan graphics API, such as `vulkaninfo`.
      pciutils # Tools for displaying and manipulating PCI devices (e.g., `lspci`).
      clinfo # A utility to query OpenCL platform and device information.
      lshw # A hardware information tool that lists detailed information about system components.
      wayland-utils # A collection of utilities for Wayland, including debugging tools.
      wl-clipboard
    ];
  };
}
