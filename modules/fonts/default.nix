{
  pkgs,
  lib,
  config,
  ...
}:
with lib;
{
  # Define new options related to fonts in the system configuration.
  options.fonts = {
    # Option to enable or disable font configuration and management system-wide.
    enable = mkEnableOption "Enable system-wide font configuration and management";

    # Define an option for setting the default font size for desktop applications.
    fontconfig.fontSize = mkOption {
      type = types.number; # The font size must be a number.
      default = 13; # Set the default font size to 13.
      description = "Default font size for desktop applications";
      example = 12; # Example font size, provided for documentation.
    };
  };

  # Apply this configuration only if font configuration is enabled.
  config = mkIf config.fonts.enable {
    fonts = {
      # Set default font families for different font types.
      fontconfig.defaultFonts = {
        sansSerif = [ "Roboto" ]; # Use Roboto as the default sans-serif font.
        monospace = [ "Hack" ]; # Use Hack as the default monospace font.
      };

      # Specify additional font packages to be installed on the system.
      packages = with pkgs; [
        roboto # Install the Roboto font.
        nerd-fonts.hack # Install Hack font from the Nerd Fonts collection,
      ];
    };
  };
}
