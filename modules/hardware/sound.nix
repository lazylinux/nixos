{
  config,
  lib,
  ...
}:
with lib;
let
  # Define a local variable `cfg` to reference the PipeWire service configuration.
  cfg = config.services.pipewire;
in
{
  # Define new options for configuring PipeWire in the system.
  options.services.pipewire = {
    # Option to set the resampling quality for PipeWire audio.
    resampleQuality = mkOption {
      type = types.int; # The value should be an integer.
      default = 15; # Default quality is set to 15 (maximum).
      description = "Resampling quality for PipeWire (0-15, higher is better quality but more CPU intensive)";
    };

    # Option to specify the maximum allowed sample rate for PipeWire in Hz.
    maxAllowedRate = mkOption {
      type = types.int; # The value should be an integer.
      default = 768000; # Default maximum sample rate set to 768000 Hz.
      description = "Maximum allowed sample rate for PipeWire in Hz";
    };
  };

  # Apply this configuration only if PipeWire is enabled in the system configuration.
  config = mkIf config.services.pipewire.enable {
    # Enable the `rtkit` daemon for real-time audio scheduling, which is recommended for PipeWire.
    security.rtkit.enable = true;

    # Additional PipeWire configuration.
    services.pipewire.extraConfig = {
      # Set the resampling quality for various PipeWire components.
      client."99-resample"."stream.properties"."resample.quality" = cfg.resampleQuality;
      client-rt."99-resample"."stream.properties"."resample.quality" = cfg.resampleQuality;
      pipewire-pulse."99-resample"."stream.properties"."resample.quality" = cfg.resampleQuality;

      # Define allowed sample rates for PipeWire, filtering them based on the maxAllowedRate setting.
      pipewire."99-allowed-rates"."context.properties"."default.clock.allowed-rates" =
        filter (rate: rate <= cfg.maxAllowedRate)
          [
            44100 # Standard CD quality
            48000 # Common sample rate for digital audio
            88200 # High-fidelity sample rate
            96000
            176400
            192000
            358000
            384000
            716000
            768000 # Highest allowed rate by default
          ];
    };
  };
}
