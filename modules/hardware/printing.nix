{
  pkgs,
  lib,
  config,
  ...
}:
# Apply this configuration only if the printing service is enabled.
lib.mkIf config.services.printing.enable {
  # Configure Avahi, a network service discovery daemon that supports the mDNS protocol,
  # which allows for networked printers and other services to be discovered automatically.
  services.avahi = {
    enable = true; # Enable the Avahi service for network discovery.
    nssmdns4 = true; # Enable mDNS for IPv4, allowing local name resolution.
    openFirewall = true; # Open firewall ports for Avahi to allow network traffic.

    # Publish services and user services on the network for discovery by other devices.
    publish = {
      enable = true; # Enable service publishing.
      userServices = true; # Publish user-level services as well.
    };
  };

  # Configure printing services with CUPS (Common Unix Printing System).
  services.printing = {
    browsing = true; # Enable printer browsing to automatically detect network printers.
    defaultShared = true; # Share printers by default, allowing other devices to use them.
    drivers = with pkgs; [
      # Install necessary printer drivers:
      # cnijfilter2 # Canon printer driver (example).
      gutenprint # Generic Gutenprint drivers for a wide range of printers.
    ];
    openFirewall = true; # Open firewall ports for printing services.
  };
}
