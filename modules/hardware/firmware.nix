{ ... }:
{
  services.fwupd.enable = true;
  hardware.enableRedistributableFirmware = true; # For some unfree drivers
  services.fstrim.enable = true;
}
