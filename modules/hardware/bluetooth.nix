{
  lib,
  host,
  config,
  ...
}:
# Apply this configuration only if Bluetooth is enabled in the system configuration.
lib.mkIf config.hardware.bluetooth.enable {
  hardware.bluetooth = {
    # Power on the Bluetooth adapter automatically at boot.
    powerOnBoot = true;

    # Bluetooth settings configuration.
    settings = {
      # General Bluetooth settings.
      General = {
        # Set the Bluetooth device name to the host name for easy identification.
        Name = "${host}";

        # Set the controller mode to "dual", which supports both Bluetooth Low Energy (LE) and Classic Bluetooth.
        ControllerMode = "dual";

        # Enable fast connectable mode, which reduces the time it takes to connect to the device.
        FastConnectable = "true";

        # Enable experimental Bluetooth features, which may add new functionality.
        Experimental = "true";
      };

      # Policy settings for Bluetooth.
      Policy = {
        # Automatically enable the Bluetooth adapter if it is not already on.
        AutoEnable = "true";
      };
    };
  };
}
