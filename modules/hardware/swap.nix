{
  lib,
  config,
  ...
}:
with lib;
{
  # Define an option for enabling swap functionality.
  options.swap.enable = mkEnableOption "Enable swap functionality. When enabled, a swap file will be created at /var/lib/swapfile with a size of 8GB.";

  # Apply this configuration only if swap is enabled.
  config = mkIf config.swap.enable {
    # Define swap devices to be used by the system.
    swapDevices = [
      {
        # Specify the location of the swap file.
        device = mkDefault "/var/lib/swapfile";

        # Set the size of the swap file to 8GB (converted to MB).
        size = mkDefault (8 * 1024); # 8GB in MB
      }
    ];
  };
}
