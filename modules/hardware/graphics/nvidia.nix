{
  lib,
  config,
  ...
}:
with lib;
{
  options.hardware.graphics.nvidia.enable =
    mkEnableOption "Whether to enable graphics for nvidia gpus";

  config = mkIf config.hardware.graphics.nvidia.enable {
    # Nvidia configuration: https://nixos.wiki/wiki/Nvidia
    # Enable Nvidia drivers for both Xorg and Wayland display servers
    services.xserver.videoDrivers = [ "nvidia" ];

    hardware.nvidia = {
      # Enable modesetting for Nvidia, necessary for the Nvidia driver to work properly
      modesetting.enable = true;

      # Disable Nvidia power management as it may cause issues with sleep/suspend functionality
      powerManagement.enable = false;

      # Disable fine-grained power management; only effective on Turing or newer Nvidia GPUs
      powerManagement.finegrained = false;

      # Use the Nvidia open-source kernel module (not to be confused with Nouveau)
      # This module is available from driver 515.43.04+ and only supports Turing and newer GPUs
      open = true;

      # Enable access to Nvidia settings through the `nvidia-settings` tool
      nvidiaSettings = true;

      # PRIME configuration for hybrid GPU setups
      prime = {
        sync.enable = true; # Enable sync to reduce tearing on hybrid GPUs
        intelBusId = "PCI:0:2:0"; # Bus ID for the integrated Intel GPU
        nvidiaBusId = "PCI:1:0:0"; # Bus ID for the Nvidia GPU
      };
    };
  };
}
