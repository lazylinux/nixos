{
  pkgs,
  lib,
  config,
  ...
}:
with lib;
{
  options.hardware.graphics.intel.enable =
    mkEnableOption "Whether to enable graphics for intel processor";

  config = mkIf config.hardware.graphics.intel.enable {
    nixpkgs.config.packageOverrides = pkgs: {
      vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
    };

    hardware.graphics = {
      enable = true;
      extraPackages = with pkgs; [
        intel-media-driver
        intel-vaapi-driver
        vaapiVdpau
        libvdpau-va-gl # Translates VDPAU calls to OpenGL
        intel-compute-runtime # OpenCL filter support (hardware tonemapping and subtitle burn-in)
        vpl-gpu-rt # QSV on 11th gen or newer
        intel-media-sdk # QSV up to 11th gen
      ];
    };
  };
}
