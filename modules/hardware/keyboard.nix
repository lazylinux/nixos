{ inputs, ... }:
{
  imports = [
    inputs.xremap-flake.nixosModules.default
  ];

  # https://discourse.nixos.org/t/best-way-to-remap-caps-lock-to-esc-with-wayland/39707/2
  # Modmap for single key rebinds
  services.xremap.config.modmap = [
    {
      name = "Global";
      remap = {
        "CapsLock" = "Esc"; # globally remap CapsLock to Esc
      };
    }
  ];

}
