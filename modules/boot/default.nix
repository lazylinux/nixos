{ pkgs, lib, ... }:
{
  boot = {
    supportedFilesystems = [ "ntfs" ];

    # Use systemd in the initramfs, which handles the early boot process.
    # This can improve boot speed and integrate well with other systemd features.
    initrd.systemd.enable = true;

    kernelPackages = pkgs.linuxPackages_latest;

    loader = {
      timeout = lib.mkDefault 0;
      # Enable systemd-boot as the EFI boot loader.
      systemd-boot = {
        enable = true;
        editor = false;
        configurationLimit = 20;
      };
      efi.canTouchEfiVariables = true;
    };

    # Silent boot settings.
    kernelParams = [
      "quiet" # Suppresses most kernel messages.
      "splash" # Enables a splash screen during boot (requires Plymouth).
      "loglevel=3" # Limits kernel messages to errors only.
      "udev.log_level=3" # Suppresses udev log messages.
      "rd.systemd.show_status=false" # Hides systemd service status updates during boot.
      "rd.udev.log_level=3" # Reduces udev log verbosity.
      "udev.log_priority=3" # Limits udev log priority to errors only.
    ];
    plymouth.enable = true;
    consoleLogLevel = 0;
    initrd.verbose = false;
  };

  # Configure shutdown timeout.
  systemd.extraConfig = ''
    DefaultTimeoutStopSec=10s
  '';
  systemd.user.extraConfig = ''
    DefaultTimeoutStopSec=10s
  '';
}
