{ user, ... }:
{
  # Define a user account with the specified username.
  # Remember to set a password for this user using the 'passwd' command after creating the account.
  users.users.${user} = {
    # Specify additional groups that this user should be a member of.
    extraGroups = [
      "networkmanager" # Allow the user to manage network connections.
      "wheel" # Grant the user administrative privileges (sudo access).
      "storage" # Enable access to storage devices.
      "video" # Allow access to video devices, such as webcams and graphics cards.
      "audio" # Enable access to audio devices for sound output.
      "camera" # Allow access to camera devices.
      "lp" # Enable access to printer devices (line printer).
      "scanner" # Allow access to scanner devices.
      "kvm" # Enable access to KVM virtualization features.
      "libvirtd" # Allow the user to manage virtual machines using libvirt.
    ];

    # Specify that this user is a normal user account (not a system account).
    isNormalUser = true;

    # Set an icon for the user account, providing a visual representation.
    icon = ./icons/siddharth.png; # Path to the user's icon image file.
  };
}
