{
  pkgs,
  lib,
  config,
  ...
}:
# Apply this configuration only if the Jellyfin service is enabled.
lib.mkIf config.services.jellyfin.enable {
  # Configuration for the Jellyfin media server service.
  services.jellyfin = {
    # Open the firewall to allow incoming connections to the Jellyfin server.
    openFirewall = true;
  };
  environment.systemPackages = with pkgs; [
    jellyfin
    jellyfin-web
    jellyfin-ffmpeg
  ];
}
