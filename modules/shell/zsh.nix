{
  config,
  pkgs,
  location,
  host,
  ...
}:
let
  nixos-rebuild-with-info = action: preCompute: ''
    ${pkgs.neofetch}/bin/neofetch
    ${preCompute} && sudo nixos-rebuild switch --show-trace --flake ${location}#${host}
  '';
in
{
  programs.zsh = {
    enable = true;

    shellAliases = {
      editNixos = "cd ${location} && nvim .";
      updateNixos = nixos-rebuild-with-info "Updating" "nix flake update --flake ${location}";
      applyNixosConfiguration = nixos-rebuild-with-info "Applying" "true";
      cat = "bat";
      cd = "z";
      l = "exa -lbF --git";
      ls = "exa -lbF --git";
      ll = "exa -lbF --git";
      la = "exa -lbhHigmuSa --time-style=long-iso --git --color-scale";
      lx = "exa -lbhHigmuSa@ --time-style=long-iso --git --color-scale";
      llt = "exa -l --git --tree";
      lt = "exa --tree --level=2";
      llm = "exa -lbGF --git --sort=modified";
      lld = "exa -lbhHFGmuSa --group-directories-first";
      nix-shell = "nix-shell --run $SHELL";
    };

    autosuggestions.enable = true;
    syntaxHighlighting.enable = true;

    histSize = 100000;
    histFile = "$HOME/.cache/zsh_history";

    ohMyZsh = {
      enable = true;
      plugins = [
        "sudo"
        "colorize"
        "aliases"
        "git"
        "systemd"
        "fzf"
        "safe-paste"
      ];
    };

    promptInit = ''
      eval "$(atuin init zsh)"
      eval "$(zoxide init zsh)"
    '';

    interactiveShellInit = ''
      source ${pkgs.zsh-fzf-tab}/share/fzf-tab/fzf-tab.plugin.zsh
      source ${pkgs.zsh-fzf-tab}/share/fzf-tab/lib/zsh-ls-colors/ls-colors.zsh
      source ${pkgs.nix-zsh-completions}/share/zsh/plugins/nix/nix-zsh-completions.plugin.zsh
      source ${pkgs.fzf}/share/fzf/completion.zsh
      source ${pkgs.zsh-vi-mode}/share/zsh-vi-mode/zsh-vi-mode.plugin.zsh source ${pkgs.fzf}/share/fzf/key-bindings.zsh
    '';
  };

  programs.starship = {
    enable = true;

    # https://starship.rs/presets/
    presets = [
      "nerd-font-symbols"
    ];
  };

  environment.systemPackages = with pkgs; [
    atuin
    zoxide
    dust
    sysz
    bat
    btop # Better htop
    eza # Better ls
    fzf
    ripgrep
  ];

  # Set Zsh as the default shell for all users
  users.defaultUserShell = pkgs.zsh;
  environment.shells = with pkgs; [ zsh ]; # Ensure Zsh is included in the list of available shells

  environment.localBinInPath = true;

  # Set environment variables for use in applications, specifically for nvim cmp-ai
  environment.variables = {
    CODESTRAL_API_KEY = "${config.sops.secrets.CODESTRAL_API_KEY.path}"; # Load API key from a secure path
  };
}
