{ pkgs, ... }:
{
  programs.tmux = {
    enable = true;
    clock24 = true;
    baseIndex = 1;
    historyLimit = 100000;
    keyMode = "vi";
    terminal = "tmux-256color";
    shortcut = "Space";
    plugins = with pkgs; [
      tmuxPlugins.catppuccin
    ];
    extraConfig = ''
      set-option -g mouse on
    '';
  };
}
