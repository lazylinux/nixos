{
  pkgs,
  lib,
  user,
  inputs,
  config,
  ...
}:
lib.mkIf config.programs.firefox.enable {
  programs.firefox = {
    package = pkgs.firefox-nightly;

    # Define a profile for the specified user with the following settings:
    profiles.${user} = {
      # Load Betterfox configurations from input files.
      extraConfig = builtins.concatStringsSep "\n" [
        (builtins.readFile "${inputs.betterfox}/user.js")
        # (builtins.readFile "${inputs.betterfox}/Securefox.js")
        # (builtins.readFile "${inputs.betterfox}/Fastfox.js")
        # (builtins.readFile "${inputs.betterfox}/Peskyfox.js")
      ];

      # Additional browser settings.
      settings = {
        "browser.download.useDownloadDir" = false;
        "extensions.pocket.enabled" = false;
        "extensions.abuseReport.enabled" = false;
        "browser.contentblocking.report.lockwise.enabled" = false;
        "browser.uitour.enabled" = false;
        "browser.newtabpage.activity-stream.showSponsored" = false;
        "browser.newtabpage.activity-stream.showSponsoredTopSites" = false;
      };
    };
  };
}
