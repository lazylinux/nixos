{
  lib,
  config,
  ...
}:
# Conditional configuration for EasyEffects settings
lib.mkIf config.services.easyeffects.enable {
  # Set up EasyEffects configuration directory
  # This directive links the configuration directory (./easyeffects)
  # into the system's XDG configuration directory structure at ~/.config/easyeffects/
  # If the `recursive` option is true, it ensures all nested files and folders within ./easyeffects are linked.
  xdg.configFile."easyeffects/" = {
    source = ./easyeffects; # Path to the EasyEffects configuration files
    recursive = true; # Ensure all files in the directory are included
  };
}
