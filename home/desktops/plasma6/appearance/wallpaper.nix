{ inputs, ... }:
let
in
{
  programs.plasma.workspace.wallpaper = inputs.wallpaper;
  programs.plasma.kscreenlocker.appearance.wallpaper = inputs.wallpaper;
}
