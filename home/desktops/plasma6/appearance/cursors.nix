{
  lib,
  config,
  ...
}:
# The Plasma cursor theme configuration.
# To see available cursor themes, run the command:
# plasma-apply-cursortheme --list-themes
lib.mkIf config.programs.plasma.enable {
  # Set the cursor theme for the Plasma workspace
  programs.plasma.workspace.cursor.theme = "Breeze_Light";
}
