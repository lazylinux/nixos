{
  lib,
  config,
  ...
}:
with lib;
let
  defaultFontType = {
    family = config.programs.plasma.fonts.fontFamily;
    pointSize = config.programs.plasma.fonts.fontSize;
  };
in
{
  options.programs.plasma.fonts.enable = mkEnableOption "";

  options.programs.plasma.fonts.fontFamily = mkOption {
    type = types.str;
    default = "Sans Serif";
    description = "Plasma font name";
  };

  options.programs.plasma.fonts.fontSize = mkOption {
    type = types.number;
    default = 13;
    description = "Plasma font size";
  };

  config = mkIf config.programs.plasma.fonts.enable {
    programs.plasma.fonts = {
      general = defaultFontType;
      small = defaultFontType // {
        pointSize = defaultFontType.pointSize - 3;
      };
      toolbar = defaultFontType;
      menu = defaultFontType;
      windowTitle = defaultFontType;
      fixedWidth = defaultFontType // {
        family = "Monospace";
      };
    };
  };
}
