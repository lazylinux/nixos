{
  lib,
  config,
  pkgs,
  ...
}:
let
  catppuccin-kde = pkgs.catppuccin-kde.override {
    flavour = [
      "mocha"
      # "macchiato"
      # "frappe"
      # "latte"
    ];
    # accents = [
    #   "rosewater"
    #   "flamingo"
    #   "pink"
    #   "mauve"
    #   "red"
    #   "maroon"
    #   "peach"
    #   "yellow"
    #   "green"
    #   "teal"
    #   "sky"
    #   "sapphire"
    #   "blue"
    #   "lavender"
    # ];
    # winDecStyles = [
    #   "modern"
    #   "classic"
    # ];
  };
in
lib.mkIf config.programs.plasma.enable {
  home.packages = [
    pkgs.lazy-packages.color-schemes # Add the color schemes package for easy theme management
    catppuccin-kde # Include the Catppuccin KDE color scheme with specified flavors
  ];

  # Use the accent color from the current wallpaper
  programs.plasma.configFile.kdeglobals.General.accentColorFromWallpaper.value = true;

  # Ensure consistent appearance in Konsole by matching the colorscheme.
  # If this value is changed, remember to update the konsole configuration in home.nix as well.

  # The Plasma colorscheme configuration.
  # To see available colorschemes, run the command:
  # plasma-apply-colorscheme --list-schemes
  programs.plasma.workspace.colorScheme = "CatppuccinMochaBlue";
}
