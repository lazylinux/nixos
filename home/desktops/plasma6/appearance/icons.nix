{
  lib,
  pkgs,
  config,
  ...
}:
let
  catppuccin-papirus-folders = pkgs.catppuccin-papirus-folders.override {
    accent = "lavender";
  };
in
lib.mkIf config.programs.plasma.enable {
  home.packages = [
    pkgs.tela-circle-icon-theme
    # pkgs.tela-icon-theme
    catppuccin-papirus-folders
  ];

  programs.plasma.workspace.iconTheme = "Tela-circle";
}
