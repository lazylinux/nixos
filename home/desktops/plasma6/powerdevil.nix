{
  lib,
  config,
  ...
}:
lib.mkIf config.programs.plasma.enable {
  programs.plasma.powerdevil.AC = {
    turnOffDisplay.idleTimeoutWhenLocked = 360; # 6 min

    powerButtonAction = "sleep";

    autoSuspend.idleTimeout = 1200; # 20 min
  };
}
