{
  lib,
  config,
  ...
}:
let
  plasmaEnabled = config.programs.plasma.enable;
  kittyEnabled = config.programs.kitty.enable;
in
lib.mkIf plasmaEnabled {
  programs.plasma.shortcuts = {
    kwin = {
      # Shortcuts for directly switching to each virtual desktop.
      "Switch to Desktop 1" = "Meta+1"; # Meta key + 1 switches to Desktop 1
      "Switch to Desktop 2" = "Meta+2"; # Meta key + 2 switches to Desktop 2
      "Switch to Desktop 3" = "Meta+3"; # Meta key + 3 switches to Desktop 3
      "Switch to Desktop 4" = "Meta+4"; # Meta key + 4 switches to Desktop 4
      "Switch to Desktop 5" = "Meta+5"; # Meta key + 5 switches to Desktop 5
      "Switch to Desktop 6" = "Meta+6"; # Meta key + 6 switches to Desktop 6
      "Switch to Desktop 7" = "Meta+7"; # Meta key + 7 switches to Desktop 7

      # Shortcuts for moving the active window to a specific desktop.
      "Window to Desktop 1" = "Meta+!"; # Meta key + ! moves the window to Desktop 1
      "Window to Desktop 2" = "Meta+@"; # Meta key + @ moves the window to Desktop 2
      "Window to Desktop 3" = "Meta+#"; # Meta key + # moves the window to Desktop 3
      "Window to Desktop 4" = "Meta+$"; # Meta key + $ moves the window to Desktop 4
      "Window to Desktop 5" = "Meta+%"; # Meta key + % moves the window to Desktop 5
      "Window to Desktop 6" = "Meta+^"; # Meta key + ^ moves the window to Desktop 6
      "Window to Desktop 7" = "Meta+&"; # Meta key + & moves the window to Desktop 7
    };

    org_kde_powerdevil = {
      Sleep = "Sleep\tPause";
    };

    ksmserver = {
      "Lock Session" = [
        "Screensaver"
        "Meta+Ctrl+Alt+L"
      ];
    };

    "kitty.desktop"._launch = lib.mkIf kittyEnabled "Ctrl+Alt+T";
    "org.kde.konsole.desktop"._launch = lib.mkIf kittyEnabled "none";
  };
}
