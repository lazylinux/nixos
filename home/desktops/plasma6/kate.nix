{
  lib,
  config,
  ...
}:
# Apply this configuration only if the Plasma desktop environment is enabled.
lib.mkIf config.programs.plasma.enable {
  # Configure the Kate text editor.
  programs.kate = {
    enable = true; # Enable the Kate editor.

    # Editor-specific configuration settings.
    editor = {
      # Bracket settings to enhance coding experience.
      brackets = {
        automaticallyAddClosing = true; # Automatically add closing brackets when typing opening brackets.
        flashMatching = true; # Flash matching brackets for visual feedback.
        highlightMatching = true; # Highlight matching brackets for easier navigation.
        highlightRangeBetween = true; # Highlight the range between matching brackets.
      };

      # Font settings for the editor.
      font = {
        family = "Monospace"; # Set the font family to Monospace for better readability in code.
        pointSize = 16; # Set the font size to 16 points.
      };

      # Set the input mode to 'vi' for users familiar with vi/vim key bindings.
      # inputMode = "vi";
    };
  };
}
