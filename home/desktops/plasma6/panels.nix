{
  lib,
  config,
  ...
}:
let
  appLauncher = {
    name = "org.kde.plasma.kickoff";
    config.General.icon = "nix-snowflake"; # Set a custom icon for the button.
  };

  taskManager = {
    name = "org.kde.plasma.icontasks";
    config = {
      General.launchers = [ ]; # Don't pin any applications
    };
  };

  clock = {
    # Display time in vertical format as the panel is vertical
    # name = "split-clock"; # Use this for vertical panels
    name = "org.kde.plasma.digitalclock";
    config.Appearance = {
      enabledCalendarPlugins = "holidaysevents"; # Enable holiday events in the calendar.
      showDate = false;
    };
  };

in
lib.mkIf config.programs.plasma.enable {
  # Vertical clock
  # home.packages = [
  #   pkgs.lazy-packages.plasma-applet-split-clock
  # ];
  # The names of the widgets can usually be found in the share/plasma/plasmoids folder of the nix-package that provides the widget/plasmoid.
  programs.plasma.panels = [
    {
      alignment = "right";
      location = "right";
      hiding = "autohide";
      lengthMode = "fit";
      floating = true;

      widgets = [
        "org.kde.plasma.systemtray"
      ];
    }
    {
      alignment = "center";
      hiding = "dodgewindows";
      lengthMode = "fit";
      floating = true;

      widgets = [
        appLauncher
        "org.kde.plasma.marginsseparator"
        taskManager
        "org.kde.plasma.marginsseparator"
        clock
      ];
    }
  ];
}
