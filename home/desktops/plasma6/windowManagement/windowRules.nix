{
  lib,
  config,
  ...
}:
let
  appGroups = {
    browsers = [
      "firefox"
      "brave"
      "chrom"
      "vivaldi"
    ];
    textEditors = [
      "code"
      "kate"
      "jetbrains"
      "kwrite"
    ];
    terminals = [
      "alacritty"
      "kitty"
      "konsole"
      "xterm"
      "terminator"
    ];
    fileManagers = [
      "dolphin"
      "okular"
      "gwenview"
      "torrent"
    ];
    videoPlayers = [
      "smplayer"
      "vlc"
      "jellyfin"
    ];
    musicPlayers = [
      "lollypop"
      "audacious"
      "strawberry"
      "spotify"
      "elisa"
      "audiotube"
      "easyeffects"
      "clementine"
      "youtube music YouTube Music"
    ];
    systemSettings = [
      "systemsettings"
      "systemmonitor"
    ];
  };

  # Function to create a wmclass pattern from a list of app names
  mkWmClassPattern =
    apps: "((^|, )(" + lib.concatStringsSep "|" (map (app: ".*${app}.*") apps) + "))+$";
in
# Apply this configuration only if Plasma is enabled in the system
lib.mkIf config.programs.plasma.enable {
  programs.plasma.window-rules = [
    {
      description = "Virtual desktop for browsers";
      apply.desktops = "Desktop_1";
      match.window-class = {
        value = mkWmClassPattern appGroups.browsers;
        type = "regex";
      };
    }
    {
      description = "Virtual desktop for text editors";
      apply.desktops = "Desktop_2";
      match.window-class = {
        value = mkWmClassPattern appGroups.textEditors;
        type = "regex";
      };
    }
    {
      description = "Virtual desktop for terminals";
      apply.desktops = "Desktop_3";
      match.window-class = {
        value = mkWmClassPattern appGroups.terminals;
        type = "regex";
      };
    }
    {
      description = "Virtual desktop for files";
      apply.desktops = "Desktop_4";
      match.window-class = {
        value = mkWmClassPattern appGroups.fileManagers;
        type = "regex";
      };
    }
    {
      description = "Virtual desktop for video players";
      apply.desktops = "Desktop_5";
      match.window-class = {
        value = mkWmClassPattern appGroups.videoPlayers;
        type = "regex";
      };
    }

    {
      description = "Virtual desktop for music players";
      apply.desktops = "Desktop_6";
      match.window-class = {
        value = mkWmClassPattern appGroups.musicPlayers;
        type = "regex";
      };
    }
    {
      description = "Virtual desktop for settings";
      apply.desktops = "Desktop_7";
      match.window-class = {
        value = mkWmClassPattern appGroups.systemSettings;
        type = "regex";
      };
    }
  ];
}
