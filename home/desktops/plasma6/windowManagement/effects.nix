{
  lib,
  config,
  ...
}:
lib.mkIf config.programs.plasma.enable {
  # Open overview on screen edges (also known as "desktop grid")
  # The current setting activates the overview effect on the top-left (5) and bottom-left (7) corners
  programs.plasma.configFile.kwinrc.Effect-overview.BorderActivate = "5,7";
}
