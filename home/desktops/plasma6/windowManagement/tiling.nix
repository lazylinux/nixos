{ lib, config, ... }:
lib.mkIf config.programs.plasma.enable {
  programs.plasma.kwin.scripts.krohnkite = {
    enable = true;
    settings = {
      gaps = {
        top = 16;
        bottom = 16;
        left = 16;
        right = 16;
        tiles = 16;
      };
    };
  };
}
