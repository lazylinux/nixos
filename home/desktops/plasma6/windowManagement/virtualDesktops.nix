{
  lib,
  config,
  ...
}:
lib.mkIf config.programs.plasma.enable {
  programs.plasma.kwin.virtualDesktops = {
    rows = 7;

    names = [
      "Internet"
      "Code"
      "Terminal"
      "Files"
      "Videos"
      "Music"
      "System"
    ];
  };
}
