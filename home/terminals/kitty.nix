{
  lib,
  config,
  ...
}:
lib.mkIf config.programs.kitty.enable {
  programs.kitty = {
    font.name = "Monospace";
    font.size = 16;
    themeFile = "Catppuccin-Mocha";
  };
}
