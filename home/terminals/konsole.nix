{
  lib,
  pkgs,
  config,
  ...
}:
lib.mkIf config.programs.konsole.enable {
  # Include the 'catppuccin-konsole' theme for the terminal emulator.
  home.packages = with pkgs.lazy-packages; [ catppuccin-konsole ];

  programs.konsole = {
    defaultProfile = "plasma-manager-konsole-profile";

    profiles = {
      plasma-manager-konsole-profile = {
        # If this value is changed, ensure to update the colorSchemes/home.nix file for consistency.
        colorScheme = "catppuccin-mocha";

        font = {
          name = "Monospace";
          size = 16;
        };

        extraConfig.Scrolling.HistorySize = "1000000";
      };
    };
  };
}
