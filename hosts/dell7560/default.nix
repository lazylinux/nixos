{ inputs, ... }:
{
  imports = [
    inputs.nixos-hardware.nixosModules.dell-inspiron-7460
  ];
  system.nixos.label = "nixos-dell7560";

  # Enable Plasma 6 desktop environment
  services.desktopManager.plasma6.enable = true;

  # Enable Firefox browser as the default web browser
  programs.firefox.enable = true;

  # Enable printing services and load necessary printer drivers
  services.printing.enable = true;

  # Enable Bluetooth support for the system
  hardware.bluetooth.enable = true;

  # Enable intel graphics drivers
  hardware.graphics.intel.enable = true;

  # Set CPU frequency governor to performance mode for higher processing speed
  powerManagement.cpuFreqGovernor = "performance";

  boot.loader.timeout = 10;

  # Mount a media drive at /run/media/Media
  fileSystems."/run/media/Media" = {
    device = "/dev/disk/by-label/Media"; # Path to the device by its label
    fsType = "ntfs"; # File system type, here set to NTFS
    noCheck = true; # Skip fsck (file system check) on this drive
    options = [ "nofail" ]; # Allows system to boot normally even if the mount fails
  };
}
