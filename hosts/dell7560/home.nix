{ ... }:
{
  # Specific home configuration for the Dell 7560 system

  # Enable Plasma programs for the desktop environment
  programs.plasma.enable = true;

  # Enable Plasma-specific font settings to improve visual consistency
  programs.plasma.fonts.enable = true;

  # Enable Firefox as the default web browser
  programs.firefox.enable = true;

  # Enable EasyEffects, an audio effects tool useful for enhancing sound quality
  services.easyeffects.enable = true;
}
