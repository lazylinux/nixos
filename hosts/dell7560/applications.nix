{
  pkgs,
  ...
}:
{
  environment.systemPackages = with pkgs; [
    # Music and Media
    strawberry-qt6 # A music player and organizer based on KDE technologies (Qt6 version).
    spotify
    youtube-music # A desktop client for YouTube Music with a native feel.
    kid3 # A tool to edit ID3 tags of audio files.
    jellyfin-media-player # A media player for the Jellyfin media server.
    vlc # A versatile media player that supports a wide range of audio and video formats.

    # Office and Productivity
    libreoffice-qt # Office suite (LibreOffice) with Qt integration for better GUI support on KDE/Qt environments.

    # Utilities and Custom Scripts
    pkgs.lazy-packages.scripts # Custom scripts from lazy-packages.
    pkgs.uwal # A wallpaper utility package from lazy-packages.
    variety # A feature-rich wallpaper manager and changer.
    wget # A non-interactive command-line downloader for files over HTTP, HTTPS, and FTP.

    # System Libraries
    libgcc # A library providing GCC runtime libraries for compatibility with certain binaries.
  ];
}
