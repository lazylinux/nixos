{ inputs, outputs, ... }:
let
  user = "siddharth";
  location = "$HOME/.setup/nixos";
  system = "x86_64-linux";
  lib = inputs.nixpkgs.lib;
  ylib = inputs.nypkgs.lib.${system};

  commonSpecialArgs = host: {
    inherit
      host
      location
      user
      inputs
      outputs
      system
      ylib
      ;
  };

  personalComputer = host: {
    inherit system;

    specialArgs = commonSpecialArgs "${host}";

    modules = [
      inputs.sops-nix.nixosModules.sops # Include Sops-Nix for managing encrypted configurations.
      ./configuration.nix

      inputs.home-manager.nixosModules.home-manager
      {
        home-manager.backupFileExtension = "backup";
        home-manager.useGlobalPkgs = true;
        home-manager.useUserPackages = true;
        home-manager.extraSpecialArgs = commonSpecialArgs "${host}";

        home-manager.users.${user} = {
          imports = [
            inputs.plasma-manager.homeManagerModules.plasma-manager
            ./home.nix
          ];
        };
      }
    ];
  };

  server = host: {
    inherit system;

    specialArgs = commonSpecialArgs "${host}";

    modules = [
      inputs.sops-nix.nixosModules.sops
      ./configuration.nix
    ];
  };
in
{
  # Define NixOS configurations for specific hosts.
  dell7560 = lib.nixosSystem (personalComputer "dell7560");

  orchid = lib.nixosSystem (personalComputer "orchid");

  lanas = lib.nixosSystem (server "lanas");
}
