{
  pkgs,
  ...
}:
{
  environment.systemPackages = with pkgs; [
    aria2
    wget
    yazi
    detox
    axel
  ];
}
