{ inputs, ... }:
{

  imports = [
    inputs.nixos-hardware.nixosModules.dell-inspiron-7460
  ];

  system.nixos.label = "nixos-lanas";

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "us";
    variant = "";
  };

  hardware.graphics.intel.enable = true;

  services.jellyfin.enable = true;

  boot.loader.timeout = 10;
  boot.kernelParams = [ "consoleblank=60" ];

  fileSystems."/run/media/Media" = {
    device = "/dev/disk/by-label/Media";
    fsType = "ntfs";
    noCheck = true;
    options = [ "nofail" ];
  };

  services.openssh.enable = true;

  xdg.portal.config.common.default = "*";

  services.logind = {
    suspendKeyLongPress = "ignore";
    suspendKey = "ignore";
    rebootKeyLongPress = "ignore";
    rebootKey = "ignore";
    powerKeyLongPress = "ignore";
    powerKey = "ignore";
    lidSwitchExternalPower = "ignore";
    lidSwitchDocked = "ignore";
    lidSwitch = "ignore";
    hibernateKeyLongPress = "ignore";
    hibernateKey = "ignore";
  };

  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };
}
