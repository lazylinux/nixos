{ user, ... }:
{
  sops = {
    # Specify the default SOPS file to use for managing secrets.
    defaultSopsFile = ../../../secrets.yaml; # Path to the main secrets file.

    # Disable validation of SOPS files.
    validateSopsFiles = false; # Set to true to enable validation before decryption.

    age = {
      # Configuration for using the Age encryption method.
      generateKey = true; # Automatically generate a new Age key if one does not exist.

      # Path to the file where the Age keys will be stored.
      keyFile = "/home/${user}/.config/sops/age/keys.txt"; # Key storage location for Age.
    };

    secrets = {
      # Define a secret named CODESTRAL_API_KEY.
      CODESTRAL_API_KEY = {
        owner = "${user}"; # Specify the owner of the secret, typically the username.
      };
    };
  };
}
