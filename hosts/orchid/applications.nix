{
  pkgs,
  ...
}:
{
  environment.systemPackages = with pkgs; [
    # Music and Media
    strawberry-qt6 # A music player and organizer based on KDE technologies (Qt6 version).
    spotify
    youtube-music # A desktop client for YouTube Music with a native feel.
    kid3 # A tool to edit ID3 tags of audio files.
    jellyfin-media-player # A media player for the Jellyfin media server.
    vlc # A versatile media player that supports a wide range of audio and video formats.
    qbittorrent

    # Office and Productivity
    libreoffice-qt # Office suite (LibreOffice) with Qt integration for better GUI support on KDE/Qt environments.

    # Utilities and Custom Scripts
    lazy-packages.scripts # Custom scripts from lazy-packages.
    uwal # A wallpaper utility package from lazy-packages.
  ];

  # Default applications for various MIME types.
  # This section specifies which applications should be used to handle specific file types.
  xdg.mime.defaultApplications = {
    "application/xml" = [
      "org.kde.kate.desktop" # Use Kate as the default application for XML files.
    ];
    "text/plain" = [
      "org.kde.kate.desktop" # Use Kate as the default application for plain text files.
    ];
    "audio/.*" = [
      "org.strawberrymusicplayer.strawberry.desktop" # Use Strawberry Music Player for audio files.
    ];
    "text/html" = [ "firefox-nightly.desktop" ];
    "x-scheme-handler/http" = [ "firefox-nightly.desktop" ];
    "x-scheme-handler/https" = [ "firefox-nightly.desktop" ];
    "x-scheme-handler/mailto" = [ "firefox-nightly.desktop" ];
  };
}
