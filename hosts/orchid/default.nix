{ pkgs, ... }:
{
  services.desktopManager.plasma6.enable = true;

  programs.firefox.enable = true;
  programs.firefox.package = pkgs.firefox-beta-bin;

  programs.chromium.enable = true;

  programs.steam.enable = true;

  services.printing.enable = true;

  hardware.bluetooth.enable = true;
}
