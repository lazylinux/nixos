{ ... }:
{
  programs.plasma.enable = true;

  programs.plasma.fonts.enable = true;
}
