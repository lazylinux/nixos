{
  outputs,
  host,
  ylib,
  ...
}:
{
  # nix.nixPath = [ "nixos-config=/path/to/configuration.nix" ];
  # Uncomment and modify the above line if you need to set a custom path for your NixOS configuration.

  nixpkgs.overlays = [
    # Add overlays your own flake exports (from overlays and pkgs dir):
    outputs.overlays.additions
    outputs.overlays.modifications
  ];

  imports = ylib.umport {
    paths = [
      ../modules
      ./common
      ./${host}
    ];
    exclude = [ ./${host}/home.nix ]; # Exclude the home configuration for this host from the imports.
  };

  swap.enable = true;

  fonts.enable = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # This should match the version of the installed NixOS; do not change unless necessary.
  # Don't change the state version unless you are sure of the implications.
}
