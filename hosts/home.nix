{
  host,
  ylib,
  ...
}:
{
  imports = ylib.umport { path = ../home; } ++ [
    ./${host}/home.nix
  ];

  /*
    The home.stateVersion option does not have a default and must be set.
    This specifies the version of the NixOS home configuration state.
    It is crucial to keep this in sync with the system state version
    to ensure compatibility with the configuration.
  */
  home.stateVersion = "23.05"; # Set the home state version; do not change this unless necessary.
  # Keeping this consistent with the system's state version is important for stability.
}
