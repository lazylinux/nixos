# This file defines overlays
{ inputs, ... }:
let
  lazy-packages = inputs.lazy-packages.packages;
  firefox-nightly = inputs.firefox-nightly.packages;
  uwal = inputs.uwal.packages;
  nixvim = inputs.nixvim.packages;
in
{
  # This integrates our custom packages into pkgs, allowing them to be accessed as pkgs.lazy-packages or pkgs.firefox-nightly.
  additions = final: _prev: {
    lazy-packages = lazy-packages.${final.system};
    firefox-nightly = firefox-nightly.${final.system}.firefox-nightly-bin;
    uwal = uwal.${final.system}.default;
    nixvim = nixvim.${final.system}.default;
  };

  # This one contains whatever you want to overlay
  # You can change versions, add patches, set compilation flags, anything really.
  # https://nixos.wiki/wiki/Overlays
  modifications = final: prev: {
    # Enables the "Titlebar accent colored" feature of a color scheme for kde plasma
    catppuccin-kde = prev.catppuccin-kde.overrideAttrs (oldAttrs: {
      postInstall = ''
        sed -i "s/accentActiveTitlebar=false/accentActiveTitlebar=true/g" $out/share/color-schemes/*.colors
      '';
    });
    vivaldi = prev.vivaldi.overrideAttrs (oldAttrs: {
      dontWrapQtApps = false;
      dontPatchELF = true;
      nativeBuildInputs = oldAttrs.nativeBuildInputs ++ [ prev.kdePackages.wrapQtAppsHook ];
    });
    kdePackages = prev.kdePackages // {
      sddm = prev.kdePackages.sddm.overrideAttrs {
        src = prev.fetchFromGitHub {
          owner = "sddm";
          repo = "sddm";
          rev = "develop";
          hash = "sha256-ayxN0l8l+N71f2UR3z+9s1h287ERnuCOKm5PEVyrwvk=";
        };
      };
    };
  };
}
