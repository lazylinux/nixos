# NixOS Configuration Setup Guide

Welcome to the setup guide for my NixOS configuration! This guide will walk you through the installation process, ensuring all steps are executed in a single terminal session, with Step 4 as a mandatory part of the installation process.

## Installation

To set up NixOS with my configuration in a single terminal session, follow these steps:

```bash
# Step 1: Install Git
nix-shell -p git

# Step 2: Clone the Configuration
git clone https://gitlab.com/lazylinux/nixos.git ~/.setup/nixos

# Step 3: Navigate to the Configuration Directory
cd ~/.setup/nixos

# Step 4: Select Your Host
# Please choose your host from the following list:
# Supported Hosts: dell7560, lanas, orchid
hostname="dell7560"  # Change this to your desired hostname

# Step 5: Copy the Host Configuration (Mandatory)
# Copy the appropriate hardware configuration to your local configuration:
cp /etc/nixos/hardware-configuration.nix ./hosts/"$hostname"

# Step 6: Apply the Configuration
# Now, it's time to apply the NixOS configuration:
sudo nixos-rebuild switch --flake .#"$hostname"

# Step 7: Reboot Your System
# After applying the configuration, reboot your system to enjoy the newly set up NixOS environment:
reboot
```

In the above commands, users can modify the `hostname` variable to their desired hostname before proceeding with the setup. The list of supported hosts is provided for reference. Users can replace `"dell7560"` with `"lanas"` or `"orchid"` as needed.

## **Important Step for SOPS Decryption**

For SOPS decryption to work successfully, you **must** add the `keys.txt` file from secure storage to the following path:

```bash
/home/user/.config/sops/keys.txt
```

This step is required for decryption to work correctly during the NixOS setup.

## Usage

Here are some common tasks you may need to perform with your NixOS configuration:

1. **To update the system**, run the following command:

    ```bash
    cd ~/.setup/nixos && nix flake update
    ```

2. **To update or rebuild the system**, you can use either of the following commands, depending on your preference:

   a. Using the NixOS rebuild command:

   ```bash
   sudo nixos-rebuild switch --flake ~/.setup/nixos#"$hostname"
   ```

   b. Using a custom alias `applyNixosConfiguration`:

   ```bash
   applyNixosConfiguration
   ```

3. **To recreate the `flake.lock` file and apply the configuration**, use the newly created alias `updateNixos`:

   ```bash
   updateNixos
   ```

This alias will ensure that the `flake.lock` file is recreated and the configuration is applied in one step.

Please note that you should execute Step 2 of the installation if there are changes in your configuration files. To update the system, run Step 1 before Step 2.
